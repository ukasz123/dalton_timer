package pl.ukaszapps.daltontimer

import android.app.UiModeManager
import android.content.res.Configuration
import android.media.RingtoneManager
import android.os.Bundle
import android.util.Log
import android.view.WindowManager

import io.flutter.app.FlutterActivity
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity() : FlutterActivity() {
	private val ringtoneManager: RingtoneManager by lazy {RingtoneManager(this).apply {
		setType(RingtoneManager.TYPE_ALARM)
	}}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		GeneratedPluginRegistrant.registerWith(this)

		// awake screen plugin
		io.flutter.plugin.common.MethodChannel(flutterView, CHANNEL_NAME).setMethodCallHandler { methodCall, result ->
			when (methodCall.method) {
				METHOD_SET_AWAKE -> {
					window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
					result.success(null)
				}
				METHOD_CLEAR_AWAKE -> {
					window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
					result.success(null)
				}
				else -> {
					result.notImplemented()
				}
			}
		}
		// tv check plugin
		io.flutter.plugin.common.MethodChannel(flutterView, TV_CHECK_CHANNEL).setMethodCallHandler { methodCall, result ->
			when(methodCall.method){
				METHOD_IS_TVSET -> {
					val uiModeManager = getSystemService(UI_MODE_SERVICE) as UiModeManager
					val isTv = uiModeManager.currentModeType == Configuration.UI_MODE_TYPE_TELEVISION
					result.success(isTv)
				}
				else -> {
					result.notImplemented()
				}
			}
		}

		// select ringtone plugin
		io.flutter.plugin.common.MethodChannel(flutterView, RINGTONE_SELECT_CHANNEL).setMethodCallHandler{ methodCall, result ->
			when(methodCall.method) {
				METHOD_SELECT -> {
					val ringtoneCursor = ringtoneManager.cursor
					val ringtonesData = arrayListOf<ArrayList<String>>()
					while (ringtoneCursor.moveToNext()){
						ringtonesData.add(arrayListOf(
								ringtoneCursor.getString(RingtoneManager.TITLE_COLUMN_INDEX),
								ringtoneCursor.getString(RingtoneManager.URI_COLUMN_INDEX)+"/"+ringtoneCursor.getInt(RingtoneManager.ID_COLUMN_INDEX)
						))
					}
					result.success(ringtonesData)
				}
				else -> {
					result.notImplemented()
				}
			}
		}
	}

	companion object {
		const val CHANNEL_NAME = "pl.ukaszapps/screenwakelock"
		const val METHOD_SET_AWAKE = "setAwake"
		const val METHOD_CLEAR_AWAKE = "clearAwake"

		const val TV_CHECK_CHANNEL = "pl.ukaszapps/tvscreencheck"
		const val METHOD_IS_TVSET = "isTVSet"

		const val RINGTONE_SELECT_CHANNEL = "pl.ukaszapps/selectringtone"
		const val METHOD_SELECT = "select"
	}
}
