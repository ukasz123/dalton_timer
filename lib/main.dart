import 'package:dalton_timer/main_mobile.dart' as mobile;
import 'package:dalton_timer/main_web.dart' as web;
import 'package:flutter/foundation.dart';

Future<void> main() async {
  if (kIsWeb) {
    await web.main();
  } else {
    await mobile.main();
  }
}