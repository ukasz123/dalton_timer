import 'dart:async';
import 'dart:ui';

import 'package:flutter/widgets.dart';
import 'package:dalton_timer/intl/locales/pl.dart' as pl;
import 'package:dalton_timer/intl/locales/en.dart' as en;
import 'package:dalton_timer/intl/locales/fr.dart' as fr;
import 'package:dalton_timer/intl/locales/de.dart' as de;

const _kSupportedLanguages = [
  'en',
  'pl',
  'fr',
  'de',
];

final kSupportedLocales = _kSupportedLanguages.map((lang) => Locale(lang));

/// This is the simple localizations provider
///
/// Skipping `intl` package because small amount of strings that will be used within the app
class TimerAppLocalizations {
  final Locale locale;

  TimerAppLocalizations(this.locale);

  static TimerAppLocalizations of(BuildContext context) {
    return Localizations.of<TimerAppLocalizations>(
        context, TimerAppLocalizations);
  }

  // internal utilities
  Map<String, String> get _translate =>
      _translations[locale.languageCode] ?? _translations['en'];

  // DECLARED RESOURCES
  String get pickTime => _translate['pickTime'];

  String get enterTimeInMinutesHint => _translate['enterTimeInMinutes'];

  String get selectDuration => _translate['selectDuration'];

  String get settings => _translate['settings'];

  String get brightness => _translate['brightness'];

  String get brightnessUseLightTheme => _translate['brightnessUseLightTheme'];

  String get appearanceSection => _translate['appearanceSection'];

  String get otherSettings => _translate['otherSettings'];

  String get about => _translate['about'];

  String get aboutApp => _translate['aboutApp'];

  String get author => _translate['author'];

  String get authorName => _translate['authorName'];

  String get contactMe => _translate['contactMe'];

  String get pickFace => _translate['pickFace'];

  String get defaultFace => _translate['defaultFace'];

  String get classicFace => _translate['classicFace'];

  String get defaultFaceWithSeconds => _translate['defaultFaceWithSeconds'];

  String get defaultFaceForVisualImpaired => _translate['defaultFaceForVisualImpaired'];

  String get licencesInformation => _translate['licencesInformation'];

  String get privacyPolicy => _translate['privacyPolicy'];

  String get policyUnavailableMessage => _translate['policyUnavailableMessage'];

  String get feedbackLabel => _translate['feedbackLabel'];

  String get cannotOpenPageMessage => _translate['cannotOpenPageMessage'];

  String get pickAlarm => _translate['pickAlarm'];

  String get defaultAlarmName => _translate['defaultAlarmName'];

  String get silentAlarmName => _translate['silentAlarmName'];

  String get alarmSoundAccessNotGranted =>
      _translate['alarmSoundAccessNotGranted'];

  String get intermediateSoundSwitchTitle =>
      _translate['intermediateSoundSwitchTitle'];

  String get intermediateSoundSwitchDescription =>
      _translate['intermediateSoundSwitchDescription'];

  String get intermediateSoundRemainingTimePickerTitle =>
      _translate['intermediateSoundRemainingTimePickerTitle'];

  String get intermediateSoundRemainingTimeSelected =>
      _translate['intermediateSoundRemainingTimeSelected'];

  String get intermediateSoundVolumeLevelTitle =>
      _translate['intermediateSoundVolumeLevelTitle'];
  // donate
  String get donateOptionTitle => _translate['donateOptionTitle'];

  // timers configuration
  String get timerSettingsTitle => _translate['timerSettingsTitle'];

  String get timerSettingsChangeDefaultTimers =>
      _translate['timerSettingsChangeDefaultTimers'];

  String get visualImpared => _translate['visualImpaired'];

  String get copyrights =>
      "${_translate['copyrights']}${DateTime.now().year} $authorName";

  // TRANSLATIONS
  static Map<String, Map<String, String>> _translations = {
    'en': en.messages,
    'pl': pl.messages,
    'fr': fr.messages,
    'de': de.messages,
  };
}

class TimerAppLocalizationsDelegate
    extends LocalizationsDelegate<TimerAppLocalizations> {
  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<TimerAppLocalizations> load(Locale locale) =>
      Future.value(TimerAppLocalizations(locale));

  @override
  bool shouldReload(LocalizationsDelegate<TimerAppLocalizations> old) => false;
}
