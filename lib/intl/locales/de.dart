// Messages for Deutch
const Map<String, String> messages = {
  'pickTime': 'Bitte gewünschte Zeit wählen',
  'enterTimeInMinutes': 'Bitte die Anzahl Minuten angeben',

  // settings
  'settings': 'Einstellungen',
  'appearanceSection': 'Einstellungen',
  'brightness': 'Helligkeit',
  'brightnessUseLightTheme': 'Bitte ein helles Design anwenden',
  'otherSettings': 'andere',
  'pickFace': 'Wählen Sie das Zifferblatt',

  'defaultFace': 'Standard',
  'classicFace': 'Klassisch',
  'feedbackLabel': 'Bitte eine Bewertung schreiben',
  'cannotOpenPageMessage': 'Die Seite kann nicht geöffnet werden.',
  'pickAlarm': 'Bitte den Ton wählen',
  'defaultAlarmName': 'Standardton',
  'silentAlarmName': 'Stumm',
  'alarmSoundAccessNotGranted': 'Der Zugang nicht gewährt.',

  // about
  'about': 'Über uns',
  'aboutApp': 'Über uns',
  'author': 'Autor',
  'authorName': 'Łukasz Huculak',
  'contactMe': 'Kontakt aufnehmen',
  'licencesInformation': 'Lizenzen',
  // privacy policy
  'privacyPolicy': 'Datenschutzbestimmungen',
  'policyUnavailableMessage':
      'Datenschutzbestimmungen sind momentan nicht abrufbar.',

  //
  'copyrights': 'Copyrights © ',
};
