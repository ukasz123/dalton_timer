// Messages for Polish
const Map<String, String> messages = {
  'pickTime': 'Wybierz czas',
  'enterTimeInMinutes': 'Wpisz liczbę minut',
  'selectDuration': 'Wybierz ',

  // settings
  'settings': 'Ustawienia',
  'appearanceSection': 'Wygląd',
  'brightness': 'Jasność',
  'brightnessUseLightTheme': 'Użyj jasnego motywu',
  'otherSettings': 'Inne',
  'pickFace': 'Wybierz tarczę',
  'defaultFace': 'Domyślna',
  'classicFace': 'Klasyczna',
  'defaultFaceWithSeconds': 'Sekundnik',
  'defaultFaceForVisualImpaired': 'Dla źle widzących',
  'feedbackLabel': 'Napisz opinię',
  'cannotOpenPageMessage': 'Nie można otworzyć strony.',
  'pickAlarm': 'Wybierz dźwięk alarmu',
  'defaultAlarmName': 'Wbudowany',
  'silentAlarmName': 'Brak',
  'alarmSoundAccessNotGranted':
      'Uprawnienie do odczytu dzwonków nie zostało przyznane.',

  'intermediateSoundSwitchTitle': 'Alarm zbliżającego się końca',
  'intermediateSoundSwitchDescription': 'Przypomina, że czas się kończy.',
  'intermediateSoundRemainingTimePickerTitle': 'Wybierz czas do końca',
  'intermediateSoundRemainingTimeSelected': 'czasu',
  'intermediateSoundVolumeLevelTitle': 'Ustaw głośność',

  'donateOptionTitle': 'Przekaż darowiznę',

  //timer configration settings
  'timerSettingsTitle': 'Własny zestaw minutników',
  'timerSettingsChangeDefaultTimers': 'Wybierz wartości',
  
  'visualImpaired': 'Dla osób z zaburzeniami widzenia',

  // about
  'about': 'O aplikacji',
  'aboutApp': 'O aplikacji',
  'author': 'Autor',
  'authorName': 'Łukasz Huculak',
  'contactMe': 'Wyślij wiadomość',
  'licencesInformation': 'Wykorzystywane licencje',
  // privacy policy
  'privacyPolicy': 'Polityka prywatności',
  'policyUnavailableMessage': 'Obecnie nie można pobrać polityki prywatności.',

  //
  'copyrights': 'Copyrights © ',
};
