// Messages for English
const Map<String, String> messages = {
  'pickTime': "Pick time",
  'enterTimeInMinutes': 'Enter time in minutes',
  'selectDuration': 'Select duration',

  // settings
  'settings': 'Settings',
  'appearanceSection': 'Appearance',
  'brightness': 'Brightness',
  'brightnessUseLightTheme': 'Use light theme',
  'otherSettings': 'Other',
  'pickFace': 'Pick face',
  'defaultFace': 'Default',
  'classicFace': 'Classic',
  'defaultFaceWithSeconds': 'Seconds',
  'defaultFaceForVisualImpaired': 'For visual impaired',
  'feedbackLabel': 'Feedback or feature request',
  'cannotOpenPageMessage': 'Cannot open page.',
  'pickAlarm': 'Pick alarm tone',
  'defaultAlarmName': 'Embedded',
  'silentAlarmName': 'Silent',
  'alarmSoundAccessNotGranted':
      'Permission to read alarm tones has not been granted.',

  'intermediateSoundSwitchTitle': 'Finishing sound',
  'intermediateSoundSwitchDescription':
      'Finishing sound reminds about the time is almost finished.',
  'intermediateSoundRemainingTimePickerTitle': 'Select remaining time',
  'intermediateSoundRemainingTimeSelected': 'of time',
  'intermediateSoundVolumeLevelTitle': 'Remaining time sound volume',

  'donateOptionTitle': 'Donate',

  //timer configration settings
  'timerSettingsTitle': 'Custom timer set',
  'timerSettingsChangeDefaultTimers': 'Change timer durations',
  
  'visualImpaired': 'Visual impaired',

  // about
  'about': 'About',
  'aboutApp': 'About application',
  'author': 'Author',
  'authorName': 'Łukasz Huculak',
  'contactMe': 'Contact me',
  'licencesInformation': 'Licences',
  // privacy policy
  'privacyPolicy': 'Privacy policy',
  'policyUnavailableMessage': 'Unable to load privacy policy.',

  //
  'copyrights': 'Copyrights © ',
};
