// Messages for French
const Map<String, String> messages = {
  'pickTime': 'Veuillez sélectionner l´heure',
  'enterTimeInMinutes': 'Veuillez spécifier le nombre de minutes',

  // settings
  'settings': 'Paramètres',
  'appearanceSection': 'Apparence',
  'brightness': 'Luminosité',
  'brightnessUseLightTheme': 'Veuillez sélectionner le thème léger',
  'otherSettings': 'Autres',
  'pickFace': '[Wybierz tarczę]',
  'defaultFace': 'Par défaut',
  'classicFace': 'Classique',
  'feedbackLabel': 'Veuillez écrire un feedback',
  'cannotOpenPageMessage': 'Le site n´est pas accessible .',
  'pickAlarm': 'Veuillez sélectionner le son du réveil',
  'defaultAlarmName': 'Le son per défault',
  'silentAlarmName': 'Silencieux',
  'alarmSoundAccessNotGranted': 'L´access interdit.',

  // about
  'about': 'A propos',
  'aboutApp': 'A propos',
  'author': 'L´auteur',
  'authorName': 'Łukasz Huculak',
  'contactMe': 'Envoyez nous un message',
  'licencesInformation': 'Licences et mentions légales ',

  // privacy policy
  'privacyPolicy': 'Paramètres de confidentialité',
  'policyUnavailableMessage':
      'Paramètres de confidentialité n´est sont pas accessible.',

  //
  'copyrights': 'Copyrights © ',
};
