import 'dart:ui';

import 'package:dalton_timer/constants.dart';
import 'package:dalton_timer/intl/localizations.dart';
import 'package:dalton_timer/sound_manager.dart';
import 'package:dalton_timer/tvcheck.dart';
import 'package:dalton_timer/widgets/faces.dart';
import 'package:dalton_timer/widgets/instance_provider.dart';
import 'package:dalton_timer/widgets/theme_switcher.dart';
import 'package:dalton_timer/theme_builder.dart';
import 'package:dalton_timer/pages/time_selection.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool isTv = await isTVSet();
  runApp(MyApp(prefs, isTv));
}

class MyApp extends StatelessWidget {
  final SharedPreferences prefs;
  final bool isTV;

  MyApp(this.prefs, this.isTV) : super();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQueryData.fromWindow(window),
      child: Builder(
        builder: (builderContext) => InstanceProvider<SharedPreferences>(
              value: prefs,
              child: Builder(builder: (context) {
                var soundManager = SoundsManager(context, onTvSet: isTV);
                SoundsManager.initAlarm(context, soundManager, prefs);
                return SoundsProvider(
                  child: InstanceProvider<bool>(
                    value: isTV,
                    child: InstanceProvider<FaceBuilder>(
                      value: defaultMobileBuilder,
                                          child: ThemeSwitcher(
                        childBuilder: (c) => ThemedApp(),
                        initialTheme: appThemeBuilder(builderContext,
                            brightness:
                                prefs.getBool(SETTINGS_LIGHT_THEME) ?? false
                                    ? Brightness.light
                                    : Brightness.dark),
                      ),
                    ),
                  ),
                  sounds: soundManager,
                );
              }),
            ),
      ),
    );
  }
}

class ThemedApp extends StatelessWidget {
  const ThemedApp({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      supportedLocales: kSupportedLocales,
      localizationsDelegates: [
        TimerAppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      title: 'Dalton Timer',
      theme: ChangeTheme.of(context).getTheme(),
      home: TimeSelectionPage(),
    );
  }
}
