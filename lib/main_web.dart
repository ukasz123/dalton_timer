import 'dart:ui';

import 'package:dalton_timer/animation_state_aware_routes.dart';
import 'package:dalton_timer/constants.dart';
import 'package:dalton_timer/intl/localizations.dart';
import 'package:dalton_timer/pages/about_app.dart';
import 'package:dalton_timer/pages/timer.dart';
import 'package:dalton_timer/sound_manager.dart';
import 'package:dalton_timer/store_icons.dart';
import 'package:dalton_timer/web/services/sounds.dart';
import 'package:dalton_timer/web/utils.dart';
import 'package:dalton_timer/web/widgets/timer_picker_cells.dart';
import 'package:dalton_timer/widgets/faces.dart';
import 'package:dalton_timer/widgets/instance_provider.dart';
import 'package:dalton_timer/widgets/src_faces/patterns.dart';
import 'package:dalton_timer/widgets/timer_cell.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:soundpool/soundpool.dart';
import 'package:url_launcher/url_launcher.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(WebApp());
}

class WebApp extends StatefulWidget {
  @override
  _WebAppState createState() => _WebAppState();
}

class _WebAppState extends State<WebApp> {
  Soundpool _soundpool;

  Future<int> _soundId;

  bool forVisualImpaired = false;

  @override
  void initState() {
    _soundpool = Soundpool();
    _soundId = rootBundle
        .load("sounds/Shut_Down1.wav")
        .then((rawBytes) => _soundpool.load(rawBytes));
    super.initState();
  }

  @override
  void dispose() {
    _soundpool.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQueryData.fromWindow(window),
      child: FutureBuilder<int>(
        future: _soundId,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return SoundsProvider(
              sounds: WebSounds(snapshot.data, _soundpool),
              child: InstanceProvider<bool>(
                value: true,
                child: InstanceProvider<FaceBuilder>(
                  value: _faceBuilder,
                  child: MaterialApp(
                    theme: ThemeData(
                      primarySwatch: Colors.blueGrey,
                    ),
                    locale: preferredLocale(),
                    supportedLocales: kSupportedLocales,
                    localizationsDelegates: [
                      TimerAppLocalizationsDelegate(),
                      GlobalMaterialLocalizations.delegate,
                      GlobalWidgetsLocalizations.delegate,
                    ],
                    title: 'Dalton Timer',
                    home: _WebTimeSelectionPage(
                        visualImpaired: forVisualImpaired,
                        onVisualImpairedChanged: (newValue) => setState(() {
                              forVisualImpaired = newValue;
                            })),
                  ),
                ),
              ),
            );
          }
          return Container();
        },
      ),
    );
  }

  Widget _faceBuilder(
      {Color color, Duration duration, Duration initialDuration, int index}) {
        
    if ((index ?? -1) < 0 || !forVisualImpaired){
      return FaceWithShadow(
        color,
        duration,
        initialDuration: initialDuration,
      );
    }
    return FaceWithShadow.withPattern(
      color,
      duration,
      Patterns.all[index],
      initialDuration: initialDuration,
    );
  }
}

class _WebTimeSelectionPage extends StatefulWidget {
  _WebTimeSelectionPage({
    Key key, this.visualImpaired, this.onVisualImpairedChanged,
  }) : super(key: key);

  final bool visualImpaired;
  final ValueSetter<bool> onVisualImpairedChanged;

  @override
  _WebTimeSelectionPageState createState() => _WebTimeSelectionPageState();
}

class _WebTimeSelectionPageState extends State<_WebTimeSelectionPage> {
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var localizations = TimerAppLocalizations.of(context);
    var mediaQuery = MediaQuery.of(context);
    var launchGooglePlayPage = () => launch(GOOGLE_PLAY_URL);
    var launchGooglePlay = OutlineButton.icon(
        onPressed: launchGooglePlayPage,
        icon: Icon(StoreIcons.googlePlay),
        label: Text('Google Play'));

    var aboutAction = () {
      Navigator.of(context).push(MaterialPageRoute<AppInfo>(
          builder: (c) => AppInfo(
                simpleInfo: true,
              )));
    };
    var aboutButton = OutlineButton.icon(
        onPressed: aboutAction,
        icon: Icon(Icons.info_outline),
        label: Text(localizations.aboutApp));

    var horizontalPadding = 36.0;
    var verticalPadding = 16.0;
    var floatingButtonSize = 72.0;
    // set up values for smaller devices
    if (mediaQuery.size.width < 600) {
      launchGooglePlay = OutlineButton(
        child: Icon(StoreIcons.googlePlay),
        onPressed: launchGooglePlayPage,
      );
      aboutButton = OutlineButton(
        onPressed: aboutAction,
        child: Text(localizations.about),
      );
    }
    if (mediaQuery.size.shortestSide < 600) {
      horizontalPadding = 16.0;
      verticalPadding = 8.0;
      floatingButtonSize = 48.0;
    }
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(localizations.pickTime),
        elevation: 2,
        actions: [
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(localizations.visualImpared),
              Switch.adaptive(value: widget.visualImpaired, onChanged: widget.onVisualImpairedChanged),
            ],
          )
        ],
      ),
      body: TimerPickerCells(
        onSelection: _onDurationSelected,
        colors: kDefaultColorsOnWhite,
      ),
      bottomNavigationBar: BottomAppBar(
        color: theme.primaryColorLight,
        shape: const CircularNotchedRectangle(),
        child: Container(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: horizontalPadding, vertical: verticalPadding),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    aboutButton,
                    launchGooglePlay,
                  ],
                ),
                SizedBox(height: 4),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    TimerAppLocalizations.of(context).copyrights,
                    style: Theme.of(context).textTheme.overline,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: SizedBox(
        width: floatingButtonSize,
        height: floatingButtonSize,
        child: FittedBox(
          child: FloatingActionButton(
            onPressed: () => _onSelectCustomDurationTap(context),
            tooltip: TimerAppLocalizations.of(context).selectDuration,
            child: Icon(Icons.timelapse),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  Future<void> _onSelectCustomDurationTap(BuildContext context) async {
    var customDuration = await showDialog<Duration>(
        context: context,
        builder: (c) {
          return DurationSelectionDialog();
        });
    if (customDuration != null) {
      _onDurationSelected(
        context,
        Theme.of(context).accentColor,
        customDuration,
      );
    }
  }

  void _onDurationSelected(BuildContext context, Color color, Duration duration,
      [int index = -1]) {
    Future<bool> complete;

    final route = MaterialPageRouteExtended(
      settings: RouteSettings(),
      builder: (BuildContext context) => TimerPage(
        timerColor: color,
        initialDuration: duration,
        animationComplete: complete,
      ),
    );
    complete = route.nextAnimationCompleted;
    Navigator.of(context).push(route);
  }
}

class DurationSelectionDialog extends StatefulWidget {
  const DurationSelectionDialog({
    Key key,
  }) : super(key: key);

  @override
  _DurationSelectionDialogState createState() =>
      _DurationSelectionDialogState();
}

class _DurationSelectionDialogState extends State<DurationSelectionDialog> {
  TextEditingController controller;
  FocusNode textFieldFocusNode;
  @override
  void initState() {
    controller = TextEditingController();
    textFieldFocusNode = FocusNode();
    textFieldFocusNode.requestFocus();
    super.initState();
  }

  @override
  void dispose() {
    textFieldFocusNode.dispose();
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var localizations = TimerAppLocalizations.of(context);
    return Align(
      alignment: Alignment(0, 0.7),
      child: SimpleDialog(
        titlePadding: EdgeInsets.all(16),
        contentPadding: EdgeInsets.symmetric(horizontal: 16),
        title: Text(localizations.selectDuration),
        children: <Widget>[
          TextField(
            autofocus: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: localizations.enterTimeInMinutesHint,
            ),
            controller: controller,
            focusNode: textFieldFocusNode,
            keyboardType:
                TextInputType.numberWithOptions(decimal: false, signed: false),
            onEditingComplete: _onSubmitDuration,
          ),
          FlatButton(
              onPressed: _onSubmitDuration,
              child: Text(MaterialLocalizations.of(context).okButtonLabel))
        ],
      ),
    );
  }

  void _onSubmitDuration() {
    var text = controller.text;
    var minutes = int.tryParse(text);
    if (minutes != null) {
      Navigator.of(context).pop(Duration(minutes: minutes));
    } else {
      controller.clear();
      textFieldFocusNode.nextFocus();
      textFieldFocusNode.requestFocus();
    }
  }
}
