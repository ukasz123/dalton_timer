import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

const String GOOGLE_PLAY_URL = "https://play.google.com/store/apps/details?id=pl.ukaszapps.daltontimer";

const String SETTINGS_LIGHT_THEME = "light_theme";
const String SETTINGS_FACE = "selected_face";

const String SETTINGS_SELECTED_ALARM = "selected_alarm";

const String SETTINGS_TIMER_CONFIGURATION = "custom_timers";

// double
const String SETTINGS_TIMER_INTERMEDIATE_ALARM_REMAINING_TIME = "intermediate_alarm_remaining";
// double 
const String SETTINGS_TIMER_INTERMEDIATE_ALARM_VOLUME = "intermediate_alarm_volume";

const Color fiveMinColor = Colors.red;
final Color tenMinColor = Colors.green.shade700;
const Color fifteenMinColor = Colors.lightBlue;
final Color thirtyMinColor = Colors.amber.shade600;

const List<Duration> kDdefaultDurations = [
    Duration(minutes: 5),
    Duration(minutes: 10),
    Duration(minutes: 15),
    Duration(minutes: 30),
  ];
  
final List<Color> kDefaultColorsOnBlack = [
    fiveMinColor,
    tenMinColor,
    fifteenMinColor,
    thirtyMinColor,
  ];

final List<Color> kDefaultColorsOnWhite = [
  Colors.red.shade800,
  Colors.green.shade800,
  Colors.lightBlue.shade800,
  Colors.amber.shade900,
];

final List<Color> kDefaultColors = kDefaultColorsOnBlack;


// feature flags
const String SETTINGS_FEATURE_VISITED_FLAGS = "feature_visited";
const int FEATURE_FLAG_SETTINGS_TAPPED_ON_NEW_ALARM = 0X1;
const int FEATURE_FLAG_ALARM_SELECTION_TAPPED = 0X2;
const int FEATURE_FLAG_INTERMEDIATE_ALARM_VISITED = 0X4;

bool isFeatureMarked(SharedPreferences prefs, int feature) {
  int currentFlags = prefs.getInt(SETTINGS_FEATURE_VISITED_FLAGS) ?? 0;
  return feature & currentFlags != 0;
}

bool areAllFeaturesMarked(SharedPreferences prefs, List<int> features) {
  int currentFlags = prefs.getInt(SETTINGS_FEATURE_VISITED_FLAGS) ?? 0;
  return features.every((feature) => feature & currentFlags != 0);
}

void markFeature(SharedPreferences prefs, int feature) {
  int currentFlags = prefs.getInt(SETTINGS_FEATURE_VISITED_FLAGS) ?? 0;
  currentFlags |= feature;
  prefs.setInt(SETTINGS_FEATURE_VISITED_FLAGS, currentFlags);
}

List<int> getIntList(SharedPreferences prefs, String key) {
  final value = prefs.getStringList(key);
  if (value == null) {
    return null;
  } else {
    return value.map((sInt) => int.parse(sInt)).toList(growable: false);
  }
}

void putIntList(SharedPreferences prefs, String key, List<int> value) {
  prefs.setStringList(key, value?.map((i) => i.toString())?.toList());
}
