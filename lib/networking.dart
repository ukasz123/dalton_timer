import 'package:dalton_timer/intl/localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart';

const String _privacyPolicyUrlPart =
    'https://ukasz-apps.bitbucket.io/policies/dalton_timer/privacy-policy-';
const String _privacyPolicyUrlSuffix = '.txt';

String policyText;

Future<void> showPrivacyPolicy(BuildContext context) async {
  final l10n = TimerAppLocalizations.of(context);
  if (policyText == null) {
    var url = _privacyPolicyUrlPart +
        Localizations.localeOf(context).languageCode +
        _privacyPolicyUrlSuffix;
    var policyResponse;
    try {
      policyResponse = await http.get(url);
      if (policyResponse.statusCode != 200) {
        url = _privacyPolicyUrlPart + 'en' + _privacyPolicyUrlSuffix;
        policyResponse = await http.get(url);
      }
    } catch (e) {
      // do nothing
    }

    if (policyResponse?.statusCode == 200) {
      policyText = utf8.decode(policyResponse.bodyBytes);
    }
  }
  if (policyText != null) {
    showDialog(
        context: context,
        builder: (c) => SimpleDialog(
              title: Text(l10n.privacyPolicy),
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text(policyText),
                )
              ],
            ));
  } else {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(l10n.policyUnavailableMessage),
    ));
  }
}

const String googlePlayPage =
    'https://play.google.com/store/apps/details?id=pl.ukaszapps.daltontimer';
Future<void> showStoreOpinionsPage(BuildContext context) async {
  final l10n = TimerAppLocalizations.of(context);
  // TODO add AppStore page when published there
  String url = googlePlayPage;
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(l10n.cannotOpenPageMessage),
    ));
  }
}

const String _donatePayPalUrl = 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=XD2HUPZNEFLD4&source=url';
Future<void> openPayPalDonateUrl(BuildContext context) async {
  final l10n = TimerAppLocalizations.of(context);
  String url = _donatePayPalUrl;
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(l10n.cannotOpenPageMessage),
    ));
  }
}
