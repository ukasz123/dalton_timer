import 'dart:async';

import 'package:dalton_timer/constants.dart';
import 'package:dalton_timer/widgets/instance_provider.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:soundpool/soundpool.dart';

abstract class Sounds {
  Future playAlarm();
  Future playIntermediateAlarm();
}

class SoundsManager extends Sounds {
  Soundpool alarmPool;
  int _alarmSoundId = -1;

  SharedPreferences prefs;

  SoundsManager(BuildContext context, {bool onTvSet = false}) {
    alarmPool =
        Soundpool(streamType: onTvSet ? StreamType.music : StreamType.alarm);
    prefs = InstanceProvider.of(context);
    
  }

  Future<void> reloadAlarmSound(String uri) async {
    await clearAlarmSound();
    _alarmSoundId = await alarmPool.loadUri(uri);
  }

  Future clearAlarmSound() async {
    if (_alarmSoundId > -1) {
      await alarmPool.release();
    }
  }

  Future<void> reloadDefault(BuildContext context) async {
    await clearAlarmSound();
    _alarmSoundId = await alarmPool.load(
        await DefaultAssetBundle.of(context).load("sounds/Shut_Down1.wav"));
  }

  @override
  Future<int> playAlarm() => _playAlarmWithVolume(1.0);
  @override
  Future<int> playIntermediateAlarm() => _playAlarmWithVolume(prefs.getDouble(SETTINGS_TIMER_INTERMEDIATE_ALARM_VOLUME) ?? 0.3, 0);

  Future<int> _playAlarmWithVolume([double volume = 1.0, int repeat = 2]) async {
    if (_alarmSoundId > -1) {
      await alarmPool.setVolume(soundId: _alarmSoundId, volume: volume);
      return await alarmPool.play(_alarmSoundId, repeat: repeat);
    } else {
      return -1;
    }
  
  }

  static void initAlarm(BuildContext context, SoundsManager soundManager,
      SharedPreferences prefs) {
    List<String> selectedAlarm = prefs.getStringList(SETTINGS_SELECTED_ALARM);
    if (selectedAlarm == null) {
      soundManager.reloadDefault(context);
    } else {
      if (selectedAlarm.length == 2) {
        // the second parameter is Uri, the first is display name
        String uri = selectedAlarm[1];
        soundManager.reloadAlarmSound(uri);
      } else {
        // do nothing, silent alarm was selected
      }
    }
  }
}

class SoundsProvider extends InheritedWidget {
  const SoundsProvider({
    Key key,
    @required Widget child,
    @required this.sounds,
  })  : assert(child != null),
        super(key: key, child: child);

  final Sounds sounds;

  static Sounds of(BuildContext context) {
    return (context.dependOnInheritedWidgetOfExactType<SoundsProvider>())
        .sounds;
  }

  @override
  bool updateShouldNotify(SoundsProvider old) {
    return old.sounds != sounds;
  }
}
