import 'dart:async';
import 'dart:math';

import 'package:dalton_timer/constants.dart';
import 'package:dalton_timer/screenwakelock.dart';
import 'package:dalton_timer/sound_manager.dart';
import 'package:dalton_timer/theme_builder.dart';
import 'package:dalton_timer/widgets/faces.dart';
import 'package:dalton_timer/widgets/instance_provider.dart';
import 'package:dalton_timer/widgets/keyboard_navigation_listener.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TimerPage extends StatelessWidget {
  final Color timerColor;
  final Duration initialDuration;
  final Future<bool> animationComplete;
  final int index;

  const TimerPage(
      {Key key,
      this.timerColor,
      this.initialDuration,
      this.animationComplete,
      this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        width: double.infinity,
        height: double.infinity,
        child: new _TimerClock(
            initialDuration: initialDuration,
            timerColor: timerColor,
            pageReady: animationComplete,
            index: index),
      ),
    );
  }
}

class _TimerClock extends StatefulWidget {
  const _TimerClock({
    Key key,
    @required this.initialDuration,
    @required this.timerColor,
    @required this.pageReady,
    this.index,
  }) : super(key: key);

  final Duration initialDuration;
  final Color timerColor;
  final Future<bool> pageReady;
  final int index;

  @override
  _TimerClockState createState() {
    return new _TimerClockState();
  }
}

class _TimerClockState extends State<_TimerClock> {
  bool _running = false;

  int _minutesLeft;

  bool _pageReadyFlag;

  DateTime _initialTime;

  DateTime _finishTime;

  bool _intermediateSoundPlayed;

  Timer _timer;

  @override
  void initState() {
    super.initState();
    _running = false;
    _pageReadyFlag = false;
    _minutesLeft = widget.initialDuration.inMinutes;
    _intermediateSoundPlayed = false;

    widget.pageReady.then((value) {
      setState(() {
        _pageReadyFlag = true;
      });
    });
  }

  @override
  void dispose() {
    _running = false;
    _timer?.cancel();
    _clearScreenAwakeLock();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final appTheme = Theme.of(context);
    final screenHeight = MediaQuery.of(context).size.height;
    var currentDuration = _running
        ? _finishTime.difference(DateTime.now())
        : widget.initialDuration;
    if (currentDuration.isNegative) {
      currentDuration = Duration();
    }
    final singlePadding = paddingSingle(context);
    return KeyboardNavigationListener(
      onSelect: () {
        _startTicking();
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: screenHeight * 0.07,
          ),
          Hero(
              tag: widget.initialDuration.inMinutes,
              child: Text(
                "$_minutesLeft",
                style: appTheme.textTheme.headline2
                    .copyWith(fontWeight: FontWeight.bold),
              )),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 1.5 * singlePadding, vertical: singlePadding),
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Hero(
                    tag: widget.initialDuration,
                    child: Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: InstanceProvider.of<FaceBuilder>(context)(
                        color: widget.timerColor,
                        duration: currentDuration,
                        initialDuration: widget.initialDuration,
                        index: widget.index,
                      ),
                    ),
                  ),
                  AnimatedOpacity(
                    duration: Duration(milliseconds: 200),
                    opacity: _running || (!_pageReadyFlag) ? 0.0 : 0.36,
                    child: GestureDetector(
                      child: LayoutBuilder(
                          builder: (context, constraints) => Center(
                                child: Icon(
                                  Icons.play_circle_outline,
                                  color: appTheme.textTheme.headline2.color,
                                  size: constraints.biggest.shortestSide,
                                ),
                              )),
                      onTap: () {
                        _startTicking();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _startTicking() {
    if (!_running) {
      _setScreenAwakeLock();
      _hideSystemUIOverlays();
      _startTimer();
      setState(() {
        _initialTime = DateTime.now();
        _finishTime = _initialTime.add(widget.initialDuration);
        _running = true;
      });
    }
  }
  void _startTimer(){
    _timer = Timer.periodic(Duration(milliseconds: 250), (timer) { 
      final millisecondsLeft =
                _finishTime.difference(DateTime.now()).inMilliseconds;
            final newMinutesLeft = max((millisecondsLeft / 60000).ceil(), 0);

            if (millisecondsLeft <= 0) {
              _playAlarmSound(context);
              _restoreSystemUIOverlay();
              _timer.cancel();
              _clearScreenAwakeLock();
            }
            if (_shouldPlayIntermediateSound(context,
                initial: widget.initialDuration.inMilliseconds,
                left: millisecondsLeft)) {
              _playIntermediateAlarmSound(context);
            }
            setState(() {
              _minutesLeft = newMinutesLeft;
            });
    });
  }

  void _setScreenAwakeLock() {
    if (!kIsWeb) setAwake();
  }

  void _clearScreenAwakeLock() {
    if (!kIsWeb) clearAwake();
  }

  void _playAlarmSound(BuildContext context) {
    _intermediateSoundPlayed = true;
    SoundsProvider.of(context).playAlarm();
  }

  void _playIntermediateAlarmSound(BuildContext context) {
    _intermediateSoundPlayed = true;
    SoundsProvider.of(context).playIntermediateAlarm();
  }

  Future<void> _hideSystemUIOverlays() =>
      SystemChrome.setEnabledSystemUIOverlays([]);
  Future<void> _restoreSystemUIOverlay() =>
      SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);

  bool _shouldPlayIntermediateSound(BuildContext context,
      {@required int initial, @required int left}) {
    if (!kIsWeb) {
      SharedPreferences prefs = InstanceProvider.of(context);
      double remaining =
          prefs.getDouble(SETTINGS_TIMER_INTERMEDIATE_ALARM_REMAINING_TIME);
      return remaining != null &&
          !_intermediateSoundPlayed &&
          left <= initial * remaining;
    } else {
      return false;
    }
  }
}
