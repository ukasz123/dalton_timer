import 'package:dalton_timer/intl/localizations.dart';
import 'package:dalton_timer/networking.dart';
import 'package:flutter/material.dart';

class DonateOption extends StatelessWidget {
  final TimerAppLocalizations localizations;
  final ThemeData currentTheme;
  const DonateOption(this.localizations, this.currentTheme);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      trailing: Icon(Icons.monetization_on),
      title: Text(
        localizations.donateOptionTitle,
      ),
      onTap: ()=> openPayPalDonateUrl(context),
    );
  }
}
