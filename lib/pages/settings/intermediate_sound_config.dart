import 'package:dalton_timer/constants.dart';
import 'package:dalton_timer/intl/localizations.dart';
import 'package:dalton_timer/widgets/instance_provider.dart';
import 'package:dalton_timer/widgets/new_indicator.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IntermediateSoundSettings extends StatefulWidget {
  @override
  _IntermediateSoundSettingsState createState() =>
      _IntermediateSoundSettingsState();
}

class _IntermediateSoundSettingsState extends State<IntermediateSoundSettings> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SharedPreferences prefs = InstanceProvider.of<SharedPreferences>(context);
    double selectedRemaining =
        prefs.getDouble(SETTINGS_TIMER_INTERMEDIATE_ALARM_REMAINING_TIME);
    double selectedVolume =
        prefs.getDouble(SETTINGS_TIMER_INTERMEDIATE_ALARM_VOLUME) ?? 0.3;
    bool intermediateSoundFeatureUsed =
        isFeatureMarked(prefs, FEATURE_FLAG_INTERMEDIATE_ALARM_VISITED);
    final localizations = TimerAppLocalizations.of(context);
    bool selected = selectedRemaining != null;
    return Column(
      children: <Widget>[
        ListTile(
          title: Text(localizations.intermediateSoundSwitchTitle),
          subtitle: Text(localizations.intermediateSoundSwitchDescription),
          trailing: SizedBox(
            width: 44,
            child: NewIndicator(
              child: Checkbox(
                onChanged: null,
                value: selected,
              ),
              indicator: intermediateSoundFeatureUsed
                  ? null
                  : (c) => FittedBox(
                        child: Icon(Icons.new_releases,
                            color: Theme.of(c).accentColor),
                      ),
            ),
          ),
          onTap: () =>_switchSoundSettings(prefs),
        ),
        if (selected) ...[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Text(localizations.intermediateSoundRemainingTimePickerTitle, style: Theme.of(context).textTheme.bodyText1),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
              children: <Widget>[
                Expanded(
                                child: Slider(value: selectedRemaining, min: 0.05, max: 0.4, divisions: 7, onChanged: (v){
                    prefs.setDouble(SETTINGS_TIMER_INTERMEDIATE_ALARM_REMAINING_TIME, v);
                    setState(() {});
                  },),
                ),
                Text('${selectedRemaining.toStringAsFixed(2)}\n${localizations.intermediateSoundRemainingTimeSelected}'),
              ],
            ),
          ),
          SizedBox(height: 4,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Text(localizations.intermediateSoundVolumeLevelTitle, style: Theme.of(context).textTheme.bodyText1),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Slider(value: selectedVolume, min: 0.05, max: 1.0, onChanged: (v){
                    prefs.setDouble(SETTINGS_TIMER_INTERMEDIATE_ALARM_VOLUME, v);
                    setState(() {});
                  },),
          ),
        ],
      ],
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
    );
  }

  void _switchSoundSettings(SharedPreferences prefs) {
    double selectedRemaining =
        prefs.getDouble(SETTINGS_TIMER_INTERMEDIATE_ALARM_REMAINING_TIME);
        markFeature(prefs, FEATURE_FLAG_INTERMEDIATE_ALARM_VISITED);
    if (selectedRemaining != null){
      prefs.setDouble(SETTINGS_TIMER_INTERMEDIATE_ALARM_REMAINING_TIME, null);
      prefs.setDouble(SETTINGS_TIMER_INTERMEDIATE_ALARM_VOLUME, null);
    } else {
      prefs.setDouble(SETTINGS_TIMER_INTERMEDIATE_ALARM_REMAINING_TIME, 0.25);
      prefs.setDouble(SETTINGS_TIMER_INTERMEDIATE_ALARM_VOLUME, 1/3);
    }
    setState(() {
      
    });
  }
}
