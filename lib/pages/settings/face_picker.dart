import 'package:dalton_timer/constants.dart';
import 'package:dalton_timer/intl/localizations.dart';
import 'package:dalton_timer/widgets/faces.dart';
import 'package:dalton_timer/widgets/instance_provider.dart';
import 'package:dalton_timer/widgets/src_faces/patterns.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FacePicker extends StatefulWidget {
  final TimerAppLocalizations localizations;

  const FacePicker({Key key, this.localizations}) : super(key: key);

  @override
  FacePickerState createState() {
    return new FacePickerState();
  }
}

class FacePickerState extends State<FacePicker> {
  int _selected = 0;

  @override
  Widget build(BuildContext context) {
    final currentTheme = Theme.of(context);
    final prefs = InstanceProvider.of<SharedPreferences>(context);
    _selected = prefs.getInt(SETTINGS_FACE) ?? 0;
    return ListView(
      children: <Widget>[
        AspectRatio(
          aspectRatio: 7 / 8,
          child: FaceFrame(
            value: 0,
            selected: _selected,
            name: widget.localizations.defaultFace,
            onTap: () => _faceSelected(0, prefs),
            child:
                FaceWithShadow(currentTheme.accentColor, Duration(minutes: 15)),
          ),
        ),
        AspectRatio(
          aspectRatio: 7 / 8,
          child: FaceFrame(
            value: 3,
            selected: _selected,
            name: widget.localizations.defaultFaceForVisualImpaired,
            onTap: () => _faceSelected(3, prefs),
            child: FaceWithShadow.withPattern(currentTheme.accentColor,
                Duration(minutes: 15), Patterns.diamonds),
          ),
        ),
        AspectRatio(
          aspectRatio: 7 / 8,
          child: FaceFrame(
            value: 2,
            selected: _selected,
            name: widget.localizations.defaultFaceWithSeconds,
            onTap: () => _faceSelected(2, prefs),
            child: FaceWithShadowsAndSeconds(
              duration: Duration(minutes: 15),
              color: currentTheme.accentColor,
            ),
          ),
        ),
        AspectRatio(
          aspectRatio: 7 / 8,
          child: FaceFrame(
            value: 1,
            selected: _selected,
            name: widget.localizations.classicFace,
            onTap: () => _faceSelected(1, prefs),
            child: FaceClassic(Duration(minutes: 15)),
          ),
        ),
      ],
      scrollDirection: Axis.horizontal,
    );
  }

  void _faceSelected(int value, SharedPreferences preferences) {
    setState(() {
      preferences.setInt(SETTINGS_FACE, value);
      _selected = value;
    });
  }
}

class FaceFrame extends StatelessWidget {
  const FaceFrame({
    Key key,
    @required this.value,
    @required int selected,
    @required this.name,
    @required this.onTap,
    @required this.child,
  })  : _selected = selected,
        super(key: key);

  final int value;
  final int _selected;
  final String name;
  final Function onTap;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Expanded(child: child),
            Padding(
              padding: const EdgeInsets.only(top: 4.0),
              child: Row(
                children: <Widget>[
                  Radio<int>(
                    value: value,
                    groupValue: _selected,
                    onChanged: (_) {},
                  ),
                  Flexible(child: Text(name)),
                ],
              ),
            )
          ],
          crossAxisAlignment: CrossAxisAlignment.stretch,
        ),
      ),
    );
  }
}
