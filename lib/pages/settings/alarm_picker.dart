
import 'dart:async';

import 'package:dalton_timer/constants.dart';
import 'package:dalton_timer/intl/localizations.dart';
import 'package:dalton_timer/sound_manager.dart';
import 'package:dalton_timer/widgets/instance_provider.dart';
import 'package:dalton_timer/widgets/new_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:soundpool/soundpool.dart';

class AlarmPicker extends StatefulWidget {
  @override
  _AlarmPickerState createState() => _AlarmPickerState();
}

class _AlarmPickerState extends State<AlarmPicker> {
  @override
  Widget build(BuildContext context) {
    SharedPreferences prefs = InstanceProvider.of<SharedPreferences>(context);
    List<String> selectedAlarm = prefs.getStringList(SETTINGS_SELECTED_ALARM);
    bool alarmFeatureUsed =
        isFeatureMarked(prefs, FEATURE_FLAG_ALARM_SELECTION_TAPPED);
    final localizations = TimerAppLocalizations.of(context);
    var selectedAlarmName = localizations.defaultAlarmName;
    if (selectedAlarm != null) {
      // first element is always a name
      selectedAlarmName = selectedAlarm[0];
    }
    return ListTile(
      onTap: () => _selectAlarm(context, selectedAlarm, localizations),
      title: Text(localizations.pickAlarm),
      subtitle: Text(selectedAlarmName),
      trailing: SizedBox(
        width: 44,
        child: NewIndicator(
          child: Icon(Icons.notifications_active),
          indicator: alarmFeatureUsed
              ? null
              : (c) => FittedBox(
                    child: Icon(Icons.new_releases,
                        color: Theme.of(c).accentColor),
                  ),
        ),
      ),
    );
  }

  Future<void> _selectAlarm(BuildContext context, List<String> current,
      TimerAppLocalizations localizations) async {
    SharedPreferences preferences = InstanceProvider.of(context);
    markFeature(preferences, FEATURE_FLAG_ALARM_SELECTION_TAPPED);
    setState(() {});
    PermissionHandler handler = PermissionHandler();
    var permissionStatus =
        await handler.requestPermissions([PermissionGroup.storage]);
    if (permissionStatus[PermissionGroup.storage] == PermissionStatus.granted) {
      List<List<String>> systemAlarms = await _alarmSelectChannel
          .invokeListMethod<List<dynamic>>("select")
          .then((v) => v.map((i) => i.cast<String>()).toList());
      await showDialog(
        context: context,
        builder: (c) => new _AlarmPickerDialog(
          systemAlarms: systemAlarms,
          current: current,
          onAlarmSelected: (selected) async {
            SharedPreferences preferences = InstanceProvider.of(context);
            await preferences.setStringList(SETTINGS_SELECTED_ALARM, selected);
            SoundsManager soundsManager = SoundsProvider.of(context);
            await soundsManager.clearAlarmSound();
            SoundsManager.initAlarm(context, soundsManager, preferences);
            Navigator.of(context).pop();
          },
        ),
      );
      setState(() {});
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(localizations.alarmSoundAccessNotGranted),
      ));
    }
  }

  static var _alarmSelectChannel = MethodChannel("pl.ukaszapps/selectringtone");
}

class _AlarmPickerDialog extends StatefulWidget {
  const _AlarmPickerDialog({
    Key key,
    @required this.systemAlarms,
    @required this.current,
    @required this.onAlarmSelected,
  }) : super(key: key);

  final List<List<String>> systemAlarms;
  final List<String> current;
  final ValueSetter<List<String>> onAlarmSelected;

  @override
  _AlarmPickerDialogState createState() => _AlarmPickerDialogState();
}

class _AlarmPickerDialogState extends State<_AlarmPickerDialog> {
  Soundpool pool = Soundpool(streamType: StreamType.alarm);
  List<Completer<int>> systemAlarms;
  Completer<int> embeddedAlarm;
  List<String> current;
  List<String> silent;

  int _nowPlayingId;
  @override
  void initState() {
    super.initState();
    current = widget.current;
    _initPool();
  }

  @override
  void dispose() {
    pool.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    TimerAppLocalizations localizations = TimerAppLocalizations.of(context);
    MaterialLocalizations mL10n = MaterialLocalizations.of(context);
    if (silent == null) {
      silent = [localizations.silentAlarmName];
    }
    return AlertDialog(
      title: Text(localizations.pickAlarm),
      content: ListView(children: [
        _buildListTile(localizations.defaultAlarmName, current == null, () {
          setState(() {
            current = null;
          });
          _playSelection(embeddedAlarm);
        }),
        _buildListTile(localizations.silentAlarmName, current == silent, () {
          setState(() {
            current = silent;
          });
        }),
        ...widget.systemAlarms.map((alarm) {
          bool selected = current != null && current[0] == alarm[0];
          return _buildListTile(alarm[0], selected, () {
            int index = widget.systemAlarms.indexOf(alarm);
            setState(() {
              current = alarm;
            });
            _playSelection(systemAlarms[index]);
          });
        })
      ]),
      actions: <Widget>[
        FlatButton(
          child: Text(mL10n.okButtonLabel),
          onPressed: () async {
            if (_nowPlayingId != null && _nowPlayingId > 0) {
              await pool.stop(_nowPlayingId);
            }
            widget.onAlarmSelected(current);
          },
        ),
      ],
    );
  }

  ListTile _buildListTile(String name, bool selected, Function onTap) {
    return ListTile(
      title: Text(name),
      selected: selected,
      trailing: Icon(
          selected ? Icons.radio_button_checked : Icons.radio_button_unchecked),
      onTap: onTap,
    );
  }

  void _initPool() {
    DefaultAssetBundle.of(context)
        .load("sounds/Shut_Down1.wav")
        .then((data) => embeddedAlarm = _completerForFuture(pool.load(data)));
    systemAlarms = widget.systemAlarms
        .map((alarm) => _completerForFuture(pool.loadUri(alarm[1])))
        .toList();
  }

  Future<void> _playSelection(Completer<int> alarm) async {
    if (_nowPlayingId != null && _nowPlayingId > 0) {
      await pool.stop(_nowPlayingId);
      _nowPlayingId = null;
    }
    if (alarm.isCompleted) {
      _nowPlayingId = await pool.play(await alarm.future);
    }
  }

  Completer<int> _completerForFuture(Future<int> alarmLoadingFuture) {
    Completer<int> completer = Completer();
    alarmLoadingFuture.then((value) {
      completer.complete(value);
    }, onError: (error) {
      completer.completeError(error);
    });
    return completer;
  }
}