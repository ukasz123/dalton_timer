import 'package:dalton_timer/constants.dart';
import 'package:dalton_timer/intl/localizations.dart';
import 'package:dalton_timer/widgets/instance_provider.dart';
import 'package:dalton_timer/widgets/time_selection_dialog.dart';
import 'package:dalton_timer/widgets/timer_cell.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TimerSettings extends StatefulWidget {
  const TimerSettings({Key key}) : super(key: key);

  @override
  _TimerSettingsState createState() => _TimerSettingsState();
}

class _TimerSettingsState extends State<TimerSettings> {
  @override
  Widget build(BuildContext context) {
    final localizations = TimerAppLocalizations.of(context);
    final SharedPreferences prefs = InstanceProvider.of(context);
    final currentDurationSettings =
        getIntList(prefs, SETTINGS_TIMER_CONFIGURATION)
                ?.map((iDuration) => Duration(minutes: iDuration))
                ?.toList(growable: false) ??
            kDdefaultDurations;
    return Scaffold(
      appBar: AppBar(
        title: Text(localizations.timerSettingsTitle),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          ListTile(
            contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
            title: Text(
              localizations.timerSettingsChangeDefaultTimers,
            ),
            trailing: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                VerticalDivider(
                  thickness: 1.5,
                ),
                IconButton(
                  icon: Icon(Icons.restore),
                  onPressed: () {
                    putIntList(prefs, SETTINGS_TIMER_CONFIGURATION, null);
                    setState(() {});
                  },
                ),
              ],
            ),
          ),
          Expanded(
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                buildTimerCell(context, currentDurationSettings, 0),
                buildTimerCell(context, currentDurationSettings, 1),
              ],
            ),
          ),
          Expanded(
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                buildTimerCell(context, currentDurationSettings, 2),
                buildTimerCell(context, currentDurationSettings, 3),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildTimerCell(
      BuildContext context, List<Duration> currentDurationSettings, int index) {
    return TimerCell(
      selected: false,
      color: kDefaultColors[index],
      duration: currentDurationSettings[index],
      onTap: (context, color, duration, index) =>
          _onTimerSelected(context, color, duration).then((newValue) {
        if (newValue != null) {
          final newDurations = List.of(currentDurationSettings);
          newDurations[index] = newValue;
          final SharedPreferences prefs = InstanceProvider.of(context);
          putIntList(prefs, SETTINGS_TIMER_CONFIGURATION,
              newDurations.map((dur) => dur.inMinutes).toList());
          setState(() {});
        }
      }),
    );
  }

  Future<Duration> _onTimerSelected(
          BuildContext context, Color color, Duration current) =>
      showDialog<Duration>(
          context: context,
          barrierDismissible: true,
          builder: (c) => TimeSelectionDialog(color: color, current: current));
}
