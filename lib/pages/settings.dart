import 'dart:async';
import 'dart:io';

import 'package:dalton_timer/constants.dart';
import 'package:dalton_timer/intl/localizations.dart';
import 'package:dalton_timer/networking.dart';
import 'package:dalton_timer/pages/about_app.dart';
import 'package:dalton_timer/pages/settings/alarm_picker.dart';
import 'package:dalton_timer/pages/settings/face_picker.dart';
import 'package:dalton_timer/pages/settings/intermediate_sound_config.dart';
import 'package:dalton_timer/pages/timer_settings.dart';
import 'package:dalton_timer/widgets/instance_provider.dart';
import 'package:dalton_timer/widgets/theme_switcher.dart';
import 'package:dalton_timer/theme_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final localizations = TimerAppLocalizations.of(context);
    final currentTheme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(localizations.settings),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildTimerValuesSettings(context, localizations, currentTheme),
              SizedBox(
                height: 16.0,
              ),
              ..._buildAppearanceSettings(context, localizations, currentTheme),
              ..._buildSoundSettings(context, localizations, currentTheme),
              SizedBox(
                height: 16.0,
              ),
              ..._buildOtherSettings(context, localizations, currentTheme),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _buildAppearanceSettings(BuildContext context,
      TimerAppLocalizations localizations, ThemeData currentTheme) {
    final changeTheme = ChangeTheme.of(context);
    var widgets = <Widget>[
      Text(
        localizations.appearanceSection.toUpperCase(),
        style: currentTheme.textTheme.subtitle1,
      ),
      Divider(),
      _buildThemeOptionRow(localizations, currentTheme, changeTheme, context),
      Divider(),
      _buildFacePicker(localizations, currentTheme, context),
    ];
    return widgets;
  }

  List<Widget> _buildSoundSettings(BuildContext context,
          TimerAppLocalizations localizations, ThemeData currentTheme) =>
      <Widget>[
        if (Platform.isAndroid) ...<Widget>[
          Divider(),
          _buildSoundPicker(localizations, currentTheme, context),
        ],
        Divider(),
        _buildIntermediateSoundSettings(localizations, currentTheme, context),
      ];

  Widget _buildThemeOptionRow(TimerAppLocalizations localizations,
      ThemeData currentTheme, ChangeTheme changeTheme, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            localizations.brightness,
            style: currentTheme.textTheme.bodyText1,
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Text(localizations.brightnessUseLightTheme),
              ),
              Switch(
                value: currentTheme.brightness == Brightness.light,
                onChanged: (isLight) {
                  changeTheme.setTheme(appThemeBuilder(context,
                      brightness:
                          isLight ? Brightness.light : Brightness.dark));
                  InstanceProvider.of<SharedPreferences>(context)
                      .setBool(SETTINGS_LIGHT_THEME, isLight);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  List<Widget> _buildOtherSettings(BuildContext context,
      TimerAppLocalizations localizations, ThemeData currentTheme) {
    return <Widget>[
      Text(
        localizations.otherSettings.toUpperCase(),
        style: currentTheme.textTheme.subtitle1,
      ),
      Divider(),
      // TODO PayPal link was rejected by Google, fix that in the future maybe
      // DonateOption(localizations, currentTheme),
      // Divider(),
      Builder(
        builder: (lContext) => ListTile(
          title: Text(localizations.feedbackLabel),
          trailing: Icon(Icons.feedback),
          onTap: () => _routeToStore(lContext),
        ),
      ),
      Divider(),
      ListTile(
        title: Text(localizations.about),
        trailing: Icon(Icons.info),
        onTap: () {
          _routeToAboutPage(context);
        },
      )
    ];
  }

  void _routeToAboutPage(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute<AppInfo>(builder: (c) => AppInfo()));
  }

  Widget _buildFacePicker(TimerAppLocalizations localizations,
      ThemeData currentTheme, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            localizations.pickFace,
            style: currentTheme.textTheme.bodyText1,
          ),
          SizedBox(
            height: 160.0,
            child: FacePicker(
              localizations: localizations,
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _routeToStore(BuildContext context) async {
    await showStoreOpinionsPage(context);
  }

  Widget _buildSoundPicker(TimerAppLocalizations localizations,
      ThemeData currentTheme, BuildContext context) {
    return AlarmPicker();
  }

  Widget _buildTimerValuesSettings(BuildContext context,
      TimerAppLocalizations localizations, ThemeData currentTheme) {
    return ListTile(
      leading: Icon(Icons.access_time),
      title: Text(localizations.timerSettingsTitle),
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => new TimerSettings(),
        ));
      },
    );
  }

  Widget _buildIntermediateSoundSettings(TimerAppLocalizations localizations,
      ThemeData currentTheme, BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 150),
      child: IntermediateSoundSettings(),
    );
  }
}
