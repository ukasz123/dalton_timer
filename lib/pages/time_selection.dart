import 'dart:async';

import 'package:dalton_timer/animation_state_aware_routes.dart';
import 'package:dalton_timer/constants.dart';
import 'package:dalton_timer/intl/localizations.dart';
import 'package:dalton_timer/networking.dart';
import 'package:dalton_timer/pages/settings.dart';
import 'package:dalton_timer/pages/timer.dart';
import 'package:dalton_timer/widgets/instance_provider.dart';
import 'package:dalton_timer/widgets/keyboard_navigation_listener.dart';
import 'package:dalton_timer/widgets/new_indicator.dart';
import 'package:dalton_timer/widgets/time_selection_dialog.dart';
import 'package:dalton_timer/widgets/timer_cell.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TimeSelectionPage extends StatefulWidget {
  const TimeSelectionPage({
    Key key,
  }) : super(key: key);

  @override
  _TimeSelectionPageState createState() {
    return new _TimeSelectionPageState();
  }
}

class _TimeSelectionPageState extends State<TimeSelectionPage> {
  FocusNode fn;

  void _onDurationSelected(BuildContext context, Color color, Duration duration, int index) {
    Future<bool> complete;

    final route = MaterialPageRouteExtended(
      builder: (BuildContext context) => TimerPage(
            timerColor: color,
            initialDuration: duration,
            animationComplete: complete,
          ),
    );
    complete = route.nextAnimationCompleted;

    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    Navigator.of(context).push(route).whenComplete(() {
      SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    });
  }

  int _selectedIndex = -1;

  @override
  Widget build(BuildContext context) {
    var localizations = TimerAppLocalizations.of(context);

    SharedPreferences preferences = InstanceProvider.of(context);
    bool settingsSelected =
        isFeatureMarked(preferences, FEATURE_FLAG_SETTINGS_TAPPED_ON_NEW_ALARM);
    final theme = Theme.of(context);
    final durations = getIntList(preferences, SETTINGS_TIMER_CONFIGURATION)
            ?.map((iDuration) => Duration(minutes: iDuration))
            ?.toList(growable: false) ??
        kDdefaultDurations;
    var actions = <Widget>[
      IconButton(
        icon: Icon(Icons.timelapse),
        onPressed: () => _onCustomTime(context),
      ),
      SizedBox(
        height: 32,
        width: 40,
        child: NewIndicator(
          child: IconButton(
            icon: Icon(Icons.settings),
            onPressed: () => _onSettings(preferences),
          ),
          indicator: settingsSelected
              ? null
              : (c) => FittedBox(
                    child: Icon(
                      Icons.new_releases,
                      color: theme.accentColor,
                    ),
                  ),
        ),
      ),
    ];
    bool isTv = InstanceProvider.of<bool>(context);
    if (!isTv && fn == null){
      fn = FocusNode(debugLabel: 'Dummy');
    }
    if (isTv) {
      actions = <Widget>[
        IconButton(
          icon: Icon(
            Icons.info_outline,
            color: _selectedIndex < 0 ? theme.accentColor : theme.buttonColor,
          ),
          onPressed: () {},
        )
      ];
    }

    return KeyboardNavigationListener(
      onMove: (direction) {
        var move = 0;
        switch (direction) {
          case NavigationDirection.left:
            move = -1;
            break;
          case NavigationDirection.right:
            move = 1;
            break;
          case NavigationDirection.up:
            move = -2;
            break;
          case NavigationDirection.down:
            move = 2;
            break;
        }
        if (move != 0) {
          int nextIndex;
          if (move == -2 && (_selectedIndex == 0 || _selectedIndex == 1)) {
            nextIndex = -1;
          } else if (_selectedIndex < 0 && move == -2) {
            nextIndex = 3;
          } else {
            nextIndex = (_selectedIndex + move + 4) % 4;
          }
          setState(() {
            _selectedIndex = nextIndex;
          });
        }
      },
      onSelect: () {
        var selection = _selectedIndex;
        if (selection < 0) {
          showPrivacyPolicy(context);
        } else {
          _onDurationSelected(context, 
              kDefaultColors[selection], durations[selection], selection);
        }
      },
      focusNode: fn,
      child: Scaffold(
        appBar: AppBar(
          title: Text(localizations.pickTime),
          actions: actions,
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  TimerCell(
                    color: kDefaultColors[0],
                    duration: durations[0],
                    onTap: _onDurationSelected,
                    selected: _selectedIndex == 0,
                    index: 0,
                  ),
                  TimerCell(
                    color: kDefaultColors[1],
                    duration: durations[1],
                    onTap: _onDurationSelected,
                    selected: _selectedIndex == 1,
                    index: 1,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  TimerCell(
                    onTap: _onDurationSelected,
                    color: kDefaultColors[2],
                    duration: durations[2],
                    selected: _selectedIndex == 2,
                    index: 2,
                  ),
                  TimerCell(
                    color: kDefaultColors[3],
                    duration: durations[3],
                    onTap: _onDurationSelected,
                    selected: _selectedIndex == 3,
                    index: 3,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onCustomTime(BuildContext context) {
    var localizations = TimerAppLocalizations.of(context);
    showDialog<Duration>(
      context: context,
      builder: (BuildContext c) =>
          new TimeSelectionDialog(
            color: Theme.of(context).accentColor,
            current: Duration(minutes: 12),
            title: localizations.selectDuration,
          ),
    ).then((durationPicked) {
      if (durationPicked != null) {
        _onDurationSelected(context, Theme.of(context).accentColor, durationPicked, -1);
      }
    });
  }

  void _onSettings(SharedPreferences prefs) {
    markFeature(prefs, FEATURE_FLAG_SETTINGS_TAPPED_ON_NEW_ALARM);
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => SettingsPage(),
    )).then((value) => setState((){}));
  }
}
