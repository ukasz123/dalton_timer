import 'package:dalton_timer/intl/localizations.dart';
import 'package:dalton_timer/networking.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AppInfo extends StatelessWidget {

/// Flag to indicate whether licences should be available 
  final bool simpleInfo;

  const AppInfo({Key key, this.simpleInfo = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    TextTheme textTheme = theme.textTheme;
    TextStyle labelStyle = textTheme.subtitle1;
    TextStyle valueStyle = textTheme.bodyText1;
    TimerAppLocalizations localizations = TimerAppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(localizations.aboutApp),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Center(
                child: Text(
                  "Dalton Timer",
                  style: textTheme.headline3,
                ),
              ),
              SizedBox(
                height: 16.0,
              ),
              Text(
                localizations.author,
                style: labelStyle,
              ),
              SizedBox(
                height: 4.0,
              ),
              Text(
                localizations.authorName,
                style: valueStyle,
              ),
              Divider(),
              InkWell(
                child: SizedBox(
                  height: 56.0,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          localizations.contactMe,
                          style: labelStyle,
                        ),
                        Text('ukasz.apps@gmail.com'),
                      ],
                      mainAxisSize: MainAxisSize.min,
                    ),
                  ),
                ),
                onTap: _messageAuthor,
              ),
              if (!simpleInfo) ...[
              Divider(),
              // licences informations
              InkWell(
                child: SizedBox(
                  height: 56.0,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(localizations.licencesInformation)),
                  ),
                ),
                onTap: () => _showLegal(context),
              ),
              ],
              Divider(),
              // privacy policy
              Builder(
                              builder: (context)=> InkWell(
                  child: SizedBox(
                    height: 56.0,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(localizations.privacyPolicy)),
                    ),
                  ),
                  onTap: () => showPrivacyPolicy(context),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _messageAuthor() {
    var emailUrl = "mailto:ukasz.apps@gmail.com?subject=Message from Dalton Timer";
    launch(emailUrl);
  }

  void _showLegal(BuildContext context) async {
    final licenceText =
        await DefaultAssetBundle.of(context).loadString("LICENCE");
    showLicensePage(
        context: context,
        applicationName: "Dalton Timer",
        applicationLegalese: licenceText);
  }
}
