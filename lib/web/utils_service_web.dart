import 'dart:ui';

import 'package:dalton_timer/web/utils.dart';
// ignore: avoid_web_libraries_in_flutter
import 'dart:html' as html;

class WebUtils implements UtilsProvider {
  @override
  Locale preferredLocale() {
    var navigator = html.window.navigator;
    var languages = navigator.languages ?? <String>[];
    var language = languages.isNotEmpty ? languages[0] : navigator.language;
    if (language != null) {
      var tags = language.split('-');
      if (tags.length > 1) {
        return Locale(tags[0], tags[1]);
      } else {
        return Locale(tags[0]);
      }
    }
    return null;
  }
}

UtilsProvider getUtils() => WebUtils();
