

import 'package:dalton_timer/sound_manager.dart';
import 'package:soundpool/soundpool.dart';

class WebSounds extends Sounds {
  final int soundId;
  final Soundpool soundpool;

  WebSounds(this.soundId, this.soundpool);
  @override
  Future playAlarm() async {
    await soundpool.setVolume(soundId: soundId, volume: 1.0);
    return await soundpool.play(soundId);
  }

  @override
  Future playIntermediateAlarm() async {
    await soundpool.setVolume(soundId: soundId, volume: 0.2);
    return await soundpool.play(soundId);
  }
}