
import 'package:flutter/widgets.dart';
import 'utils_service_stub.dart'
  // ignore: uri_does_not_exist
  if (dart.library.html) 'utils_service_web.dart';

abstract class UtilsProvider {
  Locale preferredLocale();
  factory UtilsProvider() => getUtils();
}

Locale preferredLocale() {
  return UtilsProvider().preferredLocale();
}
