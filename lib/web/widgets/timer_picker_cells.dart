import 'package:dalton_timer/constants.dart';
import 'package:dalton_timer/widgets/timer_cell.dart';
import 'package:flutter/material.dart';

class TimerPickerCells extends StatelessWidget {
  final DurationSelected onSelection;
  final List<Color> _colors;

  TimerPickerCells({
    Key key,
    @required this.onSelection, List<Color> colors ,
  }) :
  this._colors = colors ?? kDefaultColors,
   super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        var cells = <Widget>[
          TimerCell(
            color: _colors[0],
            duration: kDdefaultDurations[0],
            onTap: onSelection,
            index: 0,
          ),
          TimerCell(
            color: _colors[1],
            duration: kDdefaultDurations[1],
            onTap: onSelection,
            index: 1,
          ),
          TimerCell(
            onTap: onSelection,
            color: _colors[2],
            duration: kDdefaultDurations[2],
            index: 2,
          ),
          TimerCell(
            color: _colors[3],
            duration: kDdefaultDurations[3],
            onTap: onSelection,
            index: 3,
          ),
        ];
        if (constraints.biggest.aspectRatio < 4 / 3) {
          var splitIndex = (cells.length / 2).ceil();
          var topCells = cells.sublist(0, splitIndex);
          var bottomCells = cells.sublist(splitIndex);

          return Column(
            children: <Widget>[
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.max,
                  children: topCells,
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.max,
                  children: bottomCells,
                ),
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          );
        } else {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            children: cells,
          );
        }
      },
    );
  }
}
