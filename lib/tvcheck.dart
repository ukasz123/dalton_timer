import 'dart:async';

import 'package:flutter/services.dart';

const String _TV_CHECK_CHANNEL = "pl.ukaszapps/tvscreencheck";
const String _CHECK_TV_SET_METHOD = "isTVSet";

const _tvCheckChannel = MethodChannel(_TV_CHECK_CHANNEL);

final _tvCheckValue = Completer<bool>();

Future<bool> isTVSet() async {
  if (!_tvCheckValue.isCompleted) {
    _tvCheckValue.complete(_tvCheckChannel
        .invokeMethod(_CHECK_TV_SET_METHOD)
        .then((value) => value as bool));
  }
  return _tvCheckValue.future;
}
