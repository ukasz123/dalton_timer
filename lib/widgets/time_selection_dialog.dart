
import 'package:dalton_timer/widgets/egg_timer.dart';
import 'package:flutter/material.dart';

class TimeSelectionDialog extends StatefulWidget {
  final Color color;
  final Duration current;
  final String title;
  const TimeSelectionDialog({
    Key key,
    this.color,
    this.current,
    this.title,
  }) : super(key: key);

  @override
  _TimeSelectionDialogState createState() => _TimeSelectionDialogState();
}

class _TimeSelectionDialogState extends State<TimeSelectionDialog> {
  Duration _selectedDuration;

  @override
  void initState() {
    _selectedDuration = widget.current;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: widget.title != null ? Text(widget.title) : null,
      actions: <Widget>[
        FlatButton(
          child: Text('OK'),
          onPressed: () {
            Navigator.of(context).pop(_selectedDuration);
          },
        )
      ],
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              '${_selectedDuration.inMinutes}',
              style: Theme.of(context).textTheme.headline5,
            ),
          ),
          SizedBox(
            height: 14,
          ),
          AspectRatio(
            aspectRatio: 1.0,
            child: EggTimerPicker(
              initialDuration: widget.current,
              color: widget.color,
              onDurationSelected: (duration) {
                setState(() {
                  this._selectedDuration = duration;
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
