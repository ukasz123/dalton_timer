
import 'package:flutter/widgets.dart';

class InstanceProvider<T> extends InheritedWidget {
  const InstanceProvider({
    Key key,
    @required Widget child,
    @required this.value,
  })  : assert(child != null),
        super(key: key, child: child);

  final T value;

  static T of<T>(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<InstanceProvider<T>>()
        .value;
  }

  @override
  bool updateShouldNotify(InstanceProvider old) {
    return old.value != value;
  }
}