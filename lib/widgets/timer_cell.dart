import 'package:dalton_timer/theme_builder.dart';
import 'package:dalton_timer/widgets/faces.dart';
import 'package:dalton_timer/widgets/instance_provider.dart';
import 'package:flutter/material.dart';

class TimerCell extends StatelessWidget {
  final Duration duration;
  final Color color;
  final DurationSelected onTap;
  final bool selected;
  final int index;

  const TimerCell(
      {Key key, this.duration, this.color, this.onTap, this.selected = false, this.index = -1})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final accentColor = Theme.of(context).accentColor;
    return Expanded(
      child: GestureDetector(
        onTap: () => this.onTap(context, color, duration, index),
        child: DecoratedBox(
          decoration: BoxDecoration(
              color: color.withOpacity(0.07),
              border: selected
                  ? Border.all(color: accentColor.withOpacity(0.45), width: 2.0)
                  : null,
              boxShadow: selected
                  ? [
                      BoxShadow(
                        color: accentColor.withOpacity(0.2),
                        blurRadius: 8,
                      ),
                      BoxShadow(
                        color: color.withOpacity(0.2),
                      ),
                    ]
                  : []),
          child: Stack(
            alignment: Alignment.center,
            fit: StackFit.expand,
            children: <Widget>[
              Hero(
                tag: duration,
                child: Padding(
                  padding: EdgeInsets.all(paddingSingle(context)),
                  child: InstanceProvider.of<FaceBuilder>(context)(
                      color: color, duration: duration, index: index),
                ),
              ),
              Center(
                child: Transform(
                  transform: Matrix4.translationValues(
                      0.0,
                      Theme.of(context).textTheme.headline5.fontSize * 1.25,
                      0.0),
                  child: Hero(
                    tag: duration.inMinutes,
                    child: Text(
                      "${duration.inMinutes}",
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

typedef void DurationSelected(
    BuildContext context, Color color, Duration duration, int index);
