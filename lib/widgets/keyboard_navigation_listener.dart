import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

enum NavigationDirection { left, right, up, down }

class KeyboardNavigationListener extends StatefulWidget {
  final FocusNode focusNode;

  final ValueChanged<NavigationDirection> onMove;

  final VoidCallback onSelect;

  final Widget child;

  const KeyboardNavigationListener(
      {Key key,
      this.focusNode,
      this.onMove,
      this.onSelect,
      @required this.child})
      : assert(child != null),
        super(key: key);

  @override
  _KeyboardNavigationListenerState createState() =>
      _KeyboardNavigationListenerState();
}

class _KeyboardNavigationListenerState
    extends State<KeyboardNavigationListener> {
  FocusNode _node;
  bool requestFocus;

  @override
  void initState() {
    super.initState();
    if (widget.focusNode != null) {
      _node = widget.focusNode;
      requestFocus = false;
    } else {
      _node = FocusNode();
      requestFocus = true;
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (requestFocus) {
      FocusScope.of(context).requestFocus(_node);
    }
  }

  @override
  void dispose() {
    super.dispose();
    if (requestFocus) {
      _node.dispose();
    }
  }

  @override
  Widget build(BuildContext context) {
    return RawKeyboardListener(
      focusNode: _node,
      onKey: _handleKey,
      child: widget.child,
    );
  }

  void _handleKey(RawKeyEvent event) {
    if (event is RawKeyDownEvent) {
      return;
    }
    final androidKey = event.data as RawKeyEventDataAndroid;

    final keyCode = androidKey.keyCode;
    if (_enterKeyCodes.contains(keyCode)) {
      if (widget.onSelect != null) {
        widget.onSelect();
        return;
      }
    }
    if (widget.onMove != null) {
      if (_leftKeyCodes.contains(keyCode)) {
        widget.onMove(NavigationDirection.left);
      }
      if (_rightKeyCodes.contains(keyCode)) {
        widget.onMove(NavigationDirection.right);
      }
      if (_upKeyCodes.contains(keyCode)) {
        widget.onMove(NavigationDirection.up);
      }
      if (_downKeyCodes.contains(keyCode)) {
        widget.onMove(NavigationDirection.down);
      }
    }
  }
}

List<int> _enterKeyCodes = [
  0x00000042, // KEYCODE_ENTER
  0x0000007e, // KEYCODE_MEDIA_PLAY
  0x00000017, // KEYCODE_DPAD_CENTER
  0x0000006d, // KEYCODE_BUTTON_SELECT
  0x00000060, // KEYCODE_BUTTON_A
  0x000000a0, // KEYCODE_NUMPAD_ENTER
];
List<int> _leftKeyCodes = [
  0x00000015, // KEYCODE_DPAD_LEFT
];
List<int> _rightKeyCodes = [
  0x00000016, // KEYCODE_DPAD_RIGHT
  0x0000003d, // TAB
];
List<int> _upKeyCodes = [
  0x00000013, //KEYCODE_DPAD_UP
];
List<int> _downKeyCodes = [
  0x00000014, // KEYCODE_DPAD_DOWN
];
