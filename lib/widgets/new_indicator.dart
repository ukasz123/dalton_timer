import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class NewIndicator extends StatelessWidget {
  final Widget child;
  final WidgetBuilder indicator;

  const NewIndicator({Key key, this.child, this.indicator}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        alignment: Alignment.center,
        children: [
          child,
          if (indicator != null)
            Align(
              child: FractionallySizedBox(
                child: Builder(builder: indicator),
                heightFactor: 0.25,
              ),
              alignment: Alignment(0.7, -0.47),
            ),
        ],
      ),
    );
  }
}
