
import 'dart:math';

import 'package:flutter/material.dart';

class EggTimerPicker extends StatefulWidget {
  final Color color;
  final Duration initialDuration;
  final ValueSetter<Duration> onDurationSelected;

  const EggTimerPicker(
      {Key key,
      @required this.color,
      @required this.initialDuration,
      @required this.onDurationSelected})
      : super(key: key);

  @override
  _EggTimerPickerState createState() => _EggTimerPickerState();
}

class _EggTimerPickerState extends State<EggTimerPicker> {
  Duration duration;

  @override
  void initState() {
    this.duration = widget.initialDuration;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) => GestureDetector(
        onPanStart: (dragdetals) {
          Offset startLocal = dragdetals.localPosition;
          Offset center = constraints.biggest.center(Offset.zero);
          _updateDuration(startLocal, center);
        },
        onPanUpdate: (dragUpdateDetails) {
          Offset startLocal = dragUpdateDetails.localPosition;
          Offset center = constraints.biggest.center(Offset.zero);

          _updateDuration(startLocal, center);
        },
        child: CustomPaint(
          painter: _SelectedDurationPainter(duration, widget.color),
          foregroundPainter: _ScalePainter(
              isLight: Theme.of(context).brightness == Brightness.light),
        ),
      ),
    );
  }

  void _updateDuration(Offset startLocal, Offset center) {
    double angle = (startLocal - center).direction + pi / 2;
    int minutes = (((angle / _2_PI) * 60).floor() % 60);
    if (minutes == 0) {
      minutes = 60;
    }
    setState(() {
      duration = Duration(minutes: minutes);
    });
    widget.onDurationSelected(duration);
  }
}

const double _2_PI = 2 * pi;

class _SelectedDurationPainter extends CustomPainter {
  Color color;

  Duration duration;

  double _angle;

  Paint _durationPaint;
  final startAngle = -pi / 2;

  static const Duration _HOUR = Duration(hours: 1);
  _SelectedDurationPainter(this.duration, this.color)
      : assert(duration != null),
        assert(color != null),
        _angle = _2_PI * (duration.inMinutes / _HOUR.inMinutes),
        _durationPaint = Paint()
          ..color = color
          ..style = PaintingStyle.fill;

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawArc(Rect.fromLTWH(0, 0, size.width, size.height), startAngle,
        _angle, true, _durationPaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    if (oldDelegate is _SelectedDurationPainter) {
      return oldDelegate.duration != this.duration ||
          oldDelegate.color != this.color;
    } else {
      return true;
    }
  }
}

class _ScalePainter extends CustomPainter {
  final bool isLight;
  final Paint _scaleBorderPaint;
  final Paint _scaleMainPaint;
  final Paint _scaleSecondaryPaint;

  _ScalePainter({this.isLight = false})
      : this._scaleBorderPaint = Paint()
          ..color = isLight ? Colors.black87 : Colors.white70
          ..style = PaintingStyle.stroke
          ..strokeWidth = 4.0,
        this._scaleMainPaint = Paint()
          ..color = isLight ? Colors.black87 : Colors.white70
          ..style = PaintingStyle.stroke
          ..strokeWidth = 2.0,
        this._scaleSecondaryPaint = Paint()
          ..color = isLight ? Colors.black87 : Colors.white70
          ..style = PaintingStyle.stroke
          ..strokeWidth = 1.0;

  @override
  void paint(Canvas canvas, Size size) {
    Offset center = Offset(size.width / 2, size.height / 2);
    canvas.drawCircle(center, center.dx, _scaleBorderPaint);
    canvas.save();
    Offset topCenter = Offset(size.width / 2, 2.0);
    Offset topCenterMain = topCenter.translate(0, size.height / 25);
    Offset topCenterSecondary = topCenter.translate(0, size.height / 40);
    for (var i = 0; i < 60; i++) {
      if (i % 5 == 0) {
        canvas.drawLine(topCenter, topCenterMain, _scaleMainPaint);
      } else {
        canvas.drawLine(topCenter, topCenterSecondary, _scaleSecondaryPaint);
      }
      canvas.translate(center.dx, center.dy);
      canvas.rotate(_2_PI / 60);
      canvas.translate(-center.dx, -center.dy);
    }
    canvas.restore();
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}