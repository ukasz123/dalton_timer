export 'src_faces/face_with_shadow.dart';
export 'src_faces/face_classic.dart';

import 'package:dalton_timer/constants.dart';
import 'package:dalton_timer/widgets/instance_provider.dart';
import 'package:dalton_timer/widgets/src_faces/face_classic.dart';
import 'package:dalton_timer/widgets/src_faces/face_with_shadow.dart';
import 'package:dalton_timer/widgets/src_faces/patterns.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FaceFromSettings extends StatelessWidget {
  final Color color;
  final Duration duration;
  final Duration initialDuration;

  final int index;

  const FaceFromSettings(
      {Key key, this.color, this.duration, this.initialDuration, this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final prefs = InstanceProvider.of<SharedPreferences>(context);
    final selection = prefs.getInt(SETTINGS_FACE) ?? 0;
    switch (selection) {
      case 0:
        return FaceWithShadow(
          color,
          duration,
          initialDuration: initialDuration,
        );
      case 1:
        return FaceClassic(
          duration,
          initialDuration: initialDuration,
        );
      case 2:
        return FaceWithShadowsAndSeconds(
          color: color,
          duration: duration,
          initialDuration: initialDuration,
        );
      case 3:
        if (index < 0) {
          return FaceWithShadow(color, duration,
              initialDuration: initialDuration);
        } else {
          return FaceWithShadow.withPattern(
              color, duration, Patterns.all[index],
              initialDuration: initialDuration);
        }
    }
    return null;
  }
}

typedef Widget FaceBuilder(
    {Color color, Duration duration, Duration initialDuration,
      int index});

FaceBuilder defaultMobileBuilder = (
        {Color color,
        Duration duration,
        Duration initialDuration,
        int index}) =>
    FaceFromSettings(
      color: color,
      duration: duration,
      initialDuration: initialDuration,
      index: index ?? -1,
    );
