import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

typedef void PatternPaint(Canvas canvas, Size size, Paint paint);

const _HOUR = const Duration(minutes: 60);

class PatternPainter extends CustomPainter {
  final Color color;
  final Duration initialDuration;

  final double _edgeSize;

  final Paint _paint;
  final PatternPaint pattern;

  PatternPainter(this.color, this.initialDuration, this.pattern,
      [this._edgeSize = 2.0])
      : _paint = Paint()..color = color;

  @override
  void paint(Canvas canvas, Size size) {
    final center = size.center(Offset.zero);
    final radius = size.shortestSide / 2 - _edgeSize;

    final startAngle = -pi / 2;
    // draw shadow
    final initialSweepAngle =
        (initialDuration.inMilliseconds / _HOUR.inMilliseconds) * 2 * pi;

    Path clipping = Path()
      ..moveTo(center.dx, center.dy)
      ..addArc(Rect.fromCircle(center: center, radius: radius - _edgeSize / 2),
          startAngle, initialSweepAngle)
      ..lineTo(center.dx, center.dy)
      ..close();
    canvas.clipPath(clipping);
    pattern(canvas, size, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

class Patterns {
  static final PatternPaint verticalRectangles = (canvas, size, paint) {
    var dx = size.width/15;
    var x = 0.0;
    while (x < size.width) {
      canvas.drawRect(Rect.fromLTWH(x, 0, dx, size.height), paint);
      x += dx * 2;
    }
  };
  static final PatternPaint circles = (canvas, size, paint) {
    var radius = size.shortestSide / 31;
    var step = radius * sqrt(2) * 3;
    var oddStart = step / 2;
    var y = radius;
    while (y < size.height) {
      var x = radius;
      while (x < size.width) {
        canvas.drawCircle(Offset(x, y), radius, paint);
        x += step;
      }
      y += oddStart;
      x = radius + oddStart;
      while (x < size.width) {
        canvas.drawCircle(Offset(x, y), radius, paint);
        x += step;
      }
      y += oddStart;
    }
  };

  static final PatternPaint crates = (canvas, size, paint) {
    var step = size.shortestSide / 14;
    var x = step;
    Paint linePaint = Paint()
      ..color = paint.color
      ..style = PaintingStyle.stroke
      ..strokeWidth = step / 5;
    while (x < size.longestSide + size.shortestSide) {
      canvas.drawLine(Offset(x, 0), Offset(0, x), linePaint);
      canvas.drawLine(
          Offset(size.width - x, 0), Offset(size.width, x), linePaint);
      x += step;
    }
  };

  static final PatternPaint diamonds = (canvas, size, paint) {
    var radius = size.shortestSide / 16;
    Path diamond = Path()
        ..addPolygon([
          Offset(radius, 0),
          Offset(2 * radius, radius),
          Offset(radius, 2 * radius),
          Offset(0, radius),
        ], true);
    
    var x = 0.0;
    var step = 2 * radius;
    while (x < size.width) {
      var y = 0.0;
      while (y < size.height) {
        canvas.drawPath(Path()..addPath(diamond, Offset(x, y)), paint);
        y += step;
      }
      x += step;
    }
  };

  static final PatternPaint triangles = (canvas, size, paint) {
    var radius = size.shortestSide / 15;
    Path triangle = Path()
      ..addPolygon(
          [Offset.zero, Offset(radius, radius / 3), Offset(radius / 3, radius)],
          true);

    var x = 0.0;
    var step = 2.5 * radius;
    while (x < size.width) {
      var y = 0.0;
      while (y < size.height) {
        canvas.drawPath(Path()..addPath(triangle, Offset(x, y)), paint);
        y += step;
      }
      y = step / 2;
      x += step / 2;
      while (y < size.height) {
        canvas.drawPath(Path()..addPath(triangle, Offset(x, y)), paint);
        y += step;
      }
      x += step / 2;
    }
  };

  static final all = [
    Patterns.crates,
    Patterns.circles,
    Patterns.diamonds,
    Patterns.verticalRectangles,
    Patterns.triangles,
  ];
}
