import 'dart:math';

import 'package:dalton_timer/theme_builder.dart';
import 'package:dalton_timer/widgets/src_faces/patterns.dart';
import 'package:flutter/material.dart';

class FaceWithShadow extends StatelessWidget {
  final Color color;
  final Duration duration;
  final Duration initialDuration;
  final PatternPaint pattern;

  const FaceWithShadow(this.color, this.duration,
      {Key key, this.initialDuration})
      : pattern = null,
        super(key: key);
  const FaceWithShadow.withPattern(this.color, this.duration, this.pattern,
      {Key key, this.initialDuration})
      : super();
  @override
  Widget build(BuildContext context) {
    final edgeSize = paddingSingle(context) / 4.0;
    return CustomPaint(
      foregroundPainter: LayeredPainter([
        _FaceWithShadowRemaining(color, duration, edgeSize),
        if (pattern != null) PatternPainter(
            Theme.of(context).scaffoldBackgroundColor.withOpacity(0.56),
            initialDuration ?? duration,
            pattern),
      ]),
      painter: _FaceWithShadowBackground(color, initialDuration ?? duration,
          Theme.of(context).textTheme.bodyText2.color, edgeSize),
    );
  }
}

const Duration _HOUR = Duration(hours: 1);
const double _2_PI = 2 * pi;

class _FaceWithShadowRemaining extends CustomPainter {
  final Color color;
  final Duration duration;
  final double _angle;
  final Paint _remainingPaint;

  final double _edgeSize;

  _FaceWithShadowRemaining(this.color, this.duration, [this._edgeSize = 2.0])
      : this._angle = _2_PI * (duration.inMilliseconds / _HOUR.inMilliseconds),
        _remainingPaint = Paint()
          ..color = color
          ..style = PaintingStyle.fill;

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    if (oldDelegate is _FaceWithShadowRemaining) {
      return !(oldDelegate.duration == duration && oldDelegate.color == color);
    } else {
      return true;
    }
  }

  @override
  void paint(Canvas canvas, Size size) {
    final centerX = size.width / 2;
    final centerY = size.height / 2;
    final center = Offset(centerX, centerY);
    final radius = min(centerX, centerY);
    final startAngle = -pi / 2;

    // draw duration
    canvas.drawArc(
        Rect.fromCircle(center: center, radius: radius - _edgeSize / 2),
        startAngle,
        _angle,
        true,
        _remainingPaint);

    // presenting text
    /*final aboveHalfAnHour = _angle > pi;
    Color textColor;
    String text = "${duration.inMinutes}";
    double textX;

    if (aboveHalfAnHour) {
      textColor = _lightTextColor;
      textX = centerX + radius / 2;
    } else {
      textColor = _darkTextColor;
      textX = centerX - radius / 2;
    }
    final timePainter = TextPainter(
      text: TextSpan(
        text: text,
        style: TextStyle(color: textColor, fontSize: radius / 3.0),
      ),
      textAlign: TextAlign.left,
      textDirection: TextDirection.ltr,
    );
    timePainter.layout(maxWidth: radius / 2);
    timePainter.paint(
        canvas,
        Offset(
            textX - timePainter.width / 2, centerY - timePainter.height / 2));
  */
  }
}

class _FaceWithShadowBackground extends CustomPainter {
  final Color color;
  final Paint _faceBorderPaint;
  final Duration initialDuration;
  final Paint _shadowPaint;
  final Paint _shadowEdgePaint;

  final double _edgeSize;

  _FaceWithShadowBackground(this.color, this.initialDuration,
      [Color borderColor = Colors.black, this._edgeSize = 2.0])
      : _faceBorderPaint = Paint()
          ..color = borderColor
          ..style = PaintingStyle.stroke
          ..strokeWidth = _edgeSize,
        _shadowPaint = Paint()
          ..color = color.withOpacity(0.4)
          ..style = PaintingStyle.fill,
        _shadowEdgePaint = Paint()
          ..color = color.withOpacity(0.7)
          ..style = PaintingStyle.stroke
          ..strokeWidth = _edgeSize/2;

  @override
  void paint(Canvas canvas, Size size) {
    final centerX = size.width / 2;
    final centerY = size.height / 2;
    final center = Offset(centerX, centerY);
    final radius = min(centerX, centerY);

    final startAngle = -pi / 2;
    // draw shadow
    final initialSweepAngle =
        (initialDuration.inMilliseconds / _HOUR.inMilliseconds) * 2 * pi;

    canvas.drawArc(
        Rect.fromCircle(center: center, radius: radius - _edgeSize / 2),
        startAngle,
        initialSweepAngle,
        true,
        _shadowPaint);

    canvas.drawArc(
        Rect.fromCircle(center: center, radius: radius),
        startAngle,
        initialSweepAngle,
        true,
        _shadowEdgePaint);
    // draw face border
    canvas.drawCircle(center, radius, _faceBorderPaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    final _oldPainter = (oldDelegate as _FaceWithShadowBackground);
    return !(_oldPainter.color == this.color &&
        _oldPainter.initialDuration == this.initialDuration);
  }
}

class LayeredPainter extends CustomPainter {
  final List<CustomPainter> layers;

  LayeredPainter(this.layers) : assert(layers != null);

  @override
  void paint(Canvas canvas, Size size) {
    layers.forEach((painter) {
      painter.paint(canvas, size);
    });
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    if (!(oldDelegate is LayeredPainter)) {
      return true;
    }
    var old = (oldDelegate as LayeredPainter);
    if (old.layers != this.layers) {
      return true;
    }
    for (int i = 0; i < this.layers.length; i++) {
      var n = this.layers[i];
      var p = old.layers[i];
      if (n.runtimeType != p.runtimeType) {
        return true;
      } else {
        if (n.shouldRepaint(p)) {
          return true;
        }
      }
    }
    return false;
  }
}

class FaceWithShadowsAndSeconds extends StatelessWidget {
  final Color color;
  final Duration duration;
  final Duration initialDuration;
  final Color secondsIndicatorColor;

  const FaceWithShadowsAndSeconds(
      {Key key,
      this.color,
      this.duration,
      this.initialDuration,
      this.secondsIndicatorColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final edgeSize = paddingSingle(context) / 4.0;
    final secondsColor =
        secondsIndicatorColor ?? Theme.of(context).textTheme.bodyText1.color;
    return CustomPaint(
      foregroundPainter: LayeredPainter([
        _FaceWithShadowRemaining(color, duration, edgeSize),
        _SecondsIndicator(
          secondsColor,
          Size(edgeSize * 3, edgeSize * 1.5),
          duration,
        ),
      ]),
      painter: _FaceWithShadowBackground(color, initialDuration ?? duration,
          Theme.of(context).textTheme.bodyText2.color, edgeSize),
    );
  }
}

class _SecondsIndicator extends CustomPainter {
  final Color color;
  final Size size;
  final Duration duration;
  final Paint _indicatorPaint;

  _SecondsIndicator(this.color, this.size, this.duration)
      : _indicatorPaint = Paint()..color = color;

  @override
  void paint(Canvas canvas, Size size) {
    final centerX = size.width / 2;
    final centerY = size.height / 2;
    final center = Offset(centerX, centerY);
    final radius = min(centerX, centerY) - this.size.width * 2 / 3;
    final startAngle = -pi / 2;
    final seconds = duration.inSeconds % 60;
    final currentAngle = startAngle - (2 * pi * seconds / 60);
    canvas.save();
    canvas.translate(center.dx, center.dy);
    canvas.rotate(currentAngle);
    canvas.translate(radius, 0);
    canvas.drawOval(
        Rect.fromCenter(
            center: Offset.zero,
            width: this.size.width,
            height: this.size.height),
        _indicatorPaint);
    canvas.restore();
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    final _oldPainter = (oldDelegate as _SecondsIndicator);
    return !(_oldPainter.color == this.color && _oldPainter.size == this.size);
  }
}
