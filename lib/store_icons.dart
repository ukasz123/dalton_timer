import 'package:flutter/material.dart';

class StoreIcons {
  static final googlePlay = IconData(0xe901, fontFamily: "Store-Icons");

  static final appleStore = IconData(0xe900, fontFamily: "Store-Icons");
}