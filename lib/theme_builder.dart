import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

typedef ThemeData ThemeBuilder(BuildContext context, {Brightness brightness});

ThemeBuilder appThemeBuilder = (context, {brightness = Brightness.dark}) {
  bool isLight = brightness == Brightness.light;
  Typography platformTypography = Typography.material2018(platform: defaultTargetPlatform);
  TextTheme defaultTheme =
      isLight ? platformTypography.black : platformTypography.white;
  double screenSide = MediaQuery.of(context).size.shortestSide;
  if (screenSide >= 600) {
    defaultTheme = defaultTheme.copyWith(
        headline5: defaultTheme.headline5.copyWith(fontSize: 35.0));
  }
  return ThemeData(
    primarySwatch: Colors.blueGrey,
    primaryColor: isLight ? null : Colors.blueGrey[900],
    backgroundColor: isLight ? null : Colors.grey[800],
    accentColor:
        isLight ? Colors.cyanAccent.shade700 : Colors.cyanAccent.shade100,
    brightness: isLight ? Brightness.light : Brightness.dark,
    textTheme: defaultTheme,
  );
};

/// default padding
/// scales for larger devices
double paddingSingle(BuildContext context) =>
    MediaQuery.of(context).size.shortestSide >= 600 ? 16.0 : 8.0;
